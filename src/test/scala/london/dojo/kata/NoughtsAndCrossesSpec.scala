package london.dojo.kata

import org.scalatest.{DiagrammedAssertions, FlatSpec, Matchers}
import NoughtsAndCrosses._
import GameCLI._

class NoughtsAndCrossesSpec() extends FlatSpec with Matchers with DiagrammedAssertions {

  "a game" should "start without a victor" in {

    val game = newGame
    assert(!game.gameOver)
    assert(game.victor.isEmpty)
  }

  it should "player1 starts first" in {
    val game = newGame
    assert(game.turn == game.player1)
  }

  "players" should "each have a different O/X token" in {
    val game = newGame
    List(game.player1.token, game.player2.token) should contain allOf(O, X)
    assert(game.player1.token != game.player2.token)
  }

  "The board" should "initially be empty" in {
    val game = newGame
    assert(game.board.forall(_.forall(_.isEmpty)))
  }

  "The board" can "be visualised" in {
    val game = newGame
    assert(visualize(game) ==
    """.===.===.===.
      #|   |   |   |
      #|===|===|===|
      #|   |   |   |
      #|===|===|===|
      #|   |   |   |
      #.===.===.===.""".stripMargin('#'))
  }

  "A token" can "be placed on the board" in {
    val game1 = newGame
    val game2: NoughtsAndCrosses = game1.applyMove(1, 1)
    assert(visualize(game2) ==
    """#.===.===.===.
      #|   |   |   |
      #|===|===|===|
      #|   | O |   |
      #|===|===|===|
      #|   |   |   |
      #.===.===.===.""".stripMargin('#'))
  }

  "The next turn" should "be different after a move is made" in {
    val game1 = newGame
    val firstTurn = game1.turn
    val game2: NoughtsAndCrosses = game1.applyMove(1, 1)
    game2.turn should not be firstTurn

    val game3: NoughtsAndCrosses = game2.applyMove(1, 2)
    game3.turn shouldBe firstTurn
  }

  "The correct token" should "be used for each player" in {
    val game1 = newGame
    val game2 = game1.applyMove(1, 0)
    val game3 = game2.applyMove(2, 2)
    assert(visualize(game2) ==
      """#.===.===.===.
        #|   | O |   |
        #|===|===|===|
        #|   |   |   |
        #|===|===|===|
        #|   |   |   |
        #.===.===.===.""".stripMargin('#'))
    assert(visualize(game3) ==
      """#.===.===.===.
        #|   | O |   |
        #|===|===|===|
        #|   |   |   |
        #|===|===|===|
        #|   |   | X |
        #.===.===.===.""".stripMargin('#'))
  }

  "Invalid moves" should "be detected" in {
    val game1 = newGame.applyMove(1, 1)
    game1.isValidMove(1, 1) shouldBe false
    game1.isValidMove(2, 0) shouldBe true
  }

  "Applying a second move to a square" should "not be allowed" in {
    val game1 = newGame.applyMove(1, 1)
    intercept[IllegalArgumentException] {
      val _ = game1.applyMove(1, 1)
    }
    intercept[IllegalArgumentException] {
      val _ = game1.applyMove(3, 1)
    }
    intercept[IllegalArgumentException] {
      val _ = game1.applyMove(1, 3)
    }
  }

  "A full row" should "end the game" in {
    val game = newGame.
      applyMove(0, 0).
      applyMove(1, 1).
      applyMove(1, 0).
      applyMove(2, 2).
      applyMove(2, 0)
    game.horizontalWinner shouldBe Some(game.player1.token)
    game.horizontalWinner should not be Some(game.player2.token)
    val game2 = newGame.
      applyMove(0, 0).
      applyMove(1, 1).
      applyMove(1, 2).
      applyMove(2, 2).
      applyMove(2, 0)
    assert(visualize(game2) ==
      """#.===.===.===.
        #| O |   | O |
        #|===|===|===|
        #|   | X |   |
        #|===|===|===|
        #|   | O | X |
        #.===.===.===.""".stripMargin('#'))
    game2.horizontalWinner shouldBe None
  }

  "A full column" should "end the game" in {
    val game = newGame.
      applyMove(0, 0).
      applyMove(1, 1).
      applyMove(0, 1).
      applyMove(2, 2).
      applyMove(0, 2)
    game.verticalWinner shouldBe Some(game.player1.token)
    game.verticalWinner should not be Some(game.player2.token)
    game.gameOver shouldBe true
    val game2 = newGame.
      applyMove(0, 0).
      applyMove(1, 1).
      applyMove(1, 2).
      applyMove(2, 2).
      applyMove(2, 0)
    assert(visualize(game2) ==
      """#.===.===.===.
        #| O |   | O |
        #|===|===|===|
        #|   | X |   |
        #|===|===|===|
        #|   | O | X |
        #.===.===.===.""".stripMargin('#'))
    game2.verticalWinner shouldBe None
    game2.gameOver shouldBe false
  }

  "A diagonal threesome" should "end the game" in {
    val game = newGame.
      applyMove(0, 0).
      applyMove(1, 0).
      applyMove(1, 1).
      applyMove(1, 2).
      applyMove(2, 2)
    game.diagonalWinner shouldBe Some(game.player1.token)
    game.diagonalWinner should not be Some(game.player2.token)
    game.gameOver shouldBe true
    val game2 = newGame.
      applyMove(0, 0).
      applyMove(1, 1).
      applyMove(1, 2).
      applyMove(2, 2).
      applyMove(2, 0)
    assert(visualize(game2) ==
      """#.===.===.===.
        #| O |   | O |
        #|===|===|===|
        #|   | X |   |
        #|===|===|===|
        #|   | O | X |
        #.===.===.===.""".stripMargin('#'))
    game2.diagonalWinner shouldBe None
    game2.gameOver shouldBe false
  }

  "A game that has been won" should "tell you who won" in {
    val game1 = newGame
    val endedGame = game1.
      applyMove(0, 0). // P1
      applyMove(1, 0). // P2
      applyMove(1, 1). // P1
      applyMove(0, 1). // P2
      applyMove(2, 2)  // P1
    assert(visualize(endedGame) ==
      """#.===.===.===.
        #| O | X |   |
        #|===|===|===|
        #| X | O |   |
        #|===|===|===|
        #|   |   | O |
        #.===.===.===.""".stripMargin('#'))
    endedGame.victor shouldBe Some(endedGame.player1)
    endedGame.gameOver shouldBe true
  }

  "A full board" should "end the game" in {
    val fullGame = NoughtsAndCrosses(fullBoard, HumanPlayer("1", O), HumanPlayer("2", X), HumanPlayer("1", O))
    fullGame.gameOver shouldBe true
    fullGame.victor shouldBe None
  }

  "a quick win" should "get chosen" in {
    val easyWinBoard = List(
      List(Some(X), Some(O), Some(X)),
      List(None, Some(O), Some(O)),
      List(Some(X), Some(X), Some(O)))
    val cpu = CpuPlayer("cpu", O)
    val easyWinGame = NoughtsAndCrosses(easyWinBoard, cpu, HumanPlayer("a", X), cpu)
    pending
    cpu.getMove(easyWinGame) shouldBe (0, 1)
  }

  "a list of possible moves" should "be available" in {
    val emptyGame = newGame
    emptyGame.availableMoves shouldBe Set(
      (0, 0), (1, 0), (2, 0),
      (0, 1), (1, 1), (2, 1),
      (0, 2), (1, 2), (2, 2))
    val afterMove1 = emptyGame.applyMove(1, 1)
    afterMove1.availableMoves shouldBe Set(
      (0, 0), (1, 0), (2, 0),
      (0, 1),         (2, 1),
      (0, 2), (1, 2), (2, 2))
    val afterMove2 = afterMove1.applyMove(0, 2)
    afterMove2.availableMoves shouldBe Set(
      (0, 0), (1, 0), (2, 0),
      (0, 1),         (2, 1),
              (1, 2), (2, 2))
  }

  // GameCLI tests - should these be somewhere else??

  val fullBoard: Board = List(
    List(Some(X), Some(O), Some(X)),
    List(Some(O), Some(X), Some(O)),
    List(Some(O), Some(X), Some(O)))

  def newGame: NoughtsAndCrosses = {
    new NoughtsAndCrosses(List.fill(3)(List.fill(3)(None)), HumanPlayer("1", O), HumanPlayer("2", X), HumanPlayer("1", O))
  }

}