package london.dojo.kata

import NoughtsAndCrosses._
import scala.util.{Success, Try}

object GameCLI extends App {

  case class HumanPlayer(name: String, token: Token) extends Player {
    def getMove(state: NoughtsAndCrosses) = {
      println(this.name + " please enter a move")
      val move = Try(determineCoordinates(readLine()))
      move match {
        case Success((x, y)) if state.isValidMove(x, y) => (x, y)
        case _ => {
          println("Invalid move, please try again")
          getMove(state)
        }
      }
    }
  }

  def gameLoop(state: NoughtsAndCrosses): Unit = {
    println(visualize(state))
    val(x, y) = state.turn.getMove(state)
    val updatedState = state.applyMove(x, y)
    if (!updatedState.gameOver)
      gameLoop(updatedState)
    else
      endGame(updatedState)
  }

  def endGame(state: NoughtsAndCrosses) = {
    println(visualize(state))
    if (state.victor.isDefined)
      println("Congratulations " + state.victor.get.name)
    else
      println("Game over! You both lose!")
  }

  def determineCoordinates(inp: String): (Int, Int) = {
    val row = 2 - (inp.toInt - 1) / 3
    val col = (inp.toInt - 1) % 3
    (col, row)
  }

  def visualize(game: NoughtsAndCrosses) = {
    new AsciiGameVisualizer(game).show
  }

  val player1 = HumanPlayer("Player 1", O)
  val player2 = HumanPlayer("Player 2", X)
  val initialState = NoughtsAndCrosses(List.fill(3)(List.fill(3)(None)), player1, player2, player1)

  gameLoop(initialState)

}
