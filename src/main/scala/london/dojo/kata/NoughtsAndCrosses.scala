package london.dojo.kata

import NoughtsAndCrosses._

case class NoughtsAndCrosses(board: Board, player1: Player, player2: Player, turn: Player) {

  def applyMove(x: Int, y: Int): NoughtsAndCrosses = {
    if (isValidMove(x, y)) {
      val newBoard = board.updated(y, board(y).updated(x, Some(turn.token)))
      this.copy(board = newBoard, turn = nextPlayer)
    } else throw new IllegalArgumentException("Invalid move")
  }

  def nextPlayer = if (turn == player1) player2 else player1

  def victor : Option[Player] = {
    val winnerToken: Option[Token] = horizontalWinner.orElse(verticalWinner.orElse(diagonalWinner))
    winnerToken.map(getPlayerFromToken)
  }

  def getPlayerFromToken(token: Token) : Player = {
    if (player1.token == token)
      player1
    else
      player2
  }

  def gameOver = {
    board.forall(row => row.forall(_.isDefined)) || victor.isDefined
  }

  def isValidMove(x: Int, y: Int): Boolean = {
    y >= 0 &&
      y < board.length &&
      x >= 0 &&
      x < board(0).length &&
      board(y)(x).isEmpty
  }

  def availableMoves(): Set[(Int,Int)] = {
    val ls = for {
      y <- board.indices
      x <- board(y).indices if board(y)(x).isEmpty
    } yield (x, y)
    ls.toSet
  }


  def horizontalWinner = getWinner(board)

  def getWinner(rows: Board = board): Option[Token] = {
    val singleTokenRows = rows.map(_.distinct).filter(_.length == 1)
    val validTokenRows = singleTokenRows.filterNot(_.head.isEmpty)
    if(validTokenRows.isEmpty) {
      None
    } else {
      validTokenRows.head.head
    }

  }

  def verticalWinner: Option[Token] = {
    def getColumns(index: Int): List[Square] = board.indices.toList.map(board(_)(index))
    val columns = board.indices.toList.map(getColumns)
    getWinner(columns)
  }

  def diagonalWinner: Option[Token] = {
    val diagonal1: List[Square] = board.indices.toList.map(i => board(i)(i))
    val diagonal2: List[Square] = board.indices.toList.map(i => board(i)(board.length - i - 1))
    val diagonals = List(diagonal1, diagonal2)
    getWinner(diagonals)
  }
}

object NoughtsAndCrosses {
  sealed trait Token {}
  case object X extends Token
  case object O extends Token

  type Square = Option[Token]
  type Board = List[List[Square]]

  abstract class Player {
    val token: Token
    val name: String
    def getMove(game: NoughtsAndCrosses): (Int, Int)
  }

}


case class CpuPlayer(name: String, token: Token) extends Player {
  def getMove(game: NoughtsAndCrosses): (Int, Int) = {
    ???
  }
}
